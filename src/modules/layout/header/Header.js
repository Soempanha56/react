import Container from "react-bootstrap/Container";
import Navbar from "react-bootstrap/Navbar";
import Button from "react-bootstrap/Button";
import { useAuth } from "../../auth/components/Auth";

const Header = () => {
  const { logout } = useAuth();

  return (
    <Navbar expand="lg" className="bg-body-tertiary">
      <Container fluid>
        <Navbar.Brand href="/" className="fs-1">
          <b>KiloIT</b>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll"></Navbar.Collapse>
        <Button variant="danger" onClick={logout}>
          Logout
        </Button>
      </Container>
    </Navbar>
  );
};

export default Header;
